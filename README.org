#+TITILE: SpaceAlias

Create aliases for the number of desktops in the menu bar to make it
easier for you to access and navigate your system.

[[file:https://gitlab.com/BajaCali/spacealias/-/raw/main/docs/resources/screenshot.png]]


* Installation

#+begin_src sh

  brew tap BajaCali/SpaceAlias && brew install --cask SpaceAlias

#+end_src

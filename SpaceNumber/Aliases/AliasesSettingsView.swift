import SwiftUI
import DependencyInjection
import Combine

struct AliasesSettingsView: View {
    @InjectedObservedObject private var aliases: AliasesViewModel
    @InjectedObservedObject private var spaces: SpaceIndexViewModel
    @Injected private var spacePublisher: SpacesInfoProviding
    
    @FocusState private var focusedSpace: Int?

    var body: some View {
        VStack {
            HStack(alignment: .top) {
                Text("Space Aliases")
                    .font(.title2.bold())
                    .fontDesign(.rounded)
                Spacer()
                Button {
                    spacePublisher.updateSpaces()
                } label: {
                    Label("Reload", systemImage: "arrow.counterclockwise")
                        .foregroundColor(.secondary)
                        .font(.subheadline)
                        .labelStyle(.iconOnly)
                }
                .buttonStyle(.plain)
            }
            
            Grid(alignment: .trailing) {
                ForEach(0..<min(aliases.aliases.count, spaces.spacesCount), id: \.self) { index in
                    aliasRow(at: index)
                }
            }
        }
        .onAppear {
            focusedSpace = spaces.currentSpaceIndex
        }
        .onChange(of: spaces.currentSpaceIndex) {
            focusedSpace = $0
        }
    }

    private func aliasRow(at aliasIndex: Int) -> some View {
        let aliasBinding = $aliases.aliases[aliasIndex]

        return GridRow {
            Text("Desktop \(aliasIndex + 1)")
            TextField(String(aliasIndex + 1), text: aliasBinding)
                .textFieldStyle(.roundedBorder)
                .focused($focusedSpace, equals: aliasIndex)
        }
    }
}

struct AliasesSettingsView_Previews: PreviewProvider {
    static var previews: some View {
        AliasesSettingsView()
    }
}

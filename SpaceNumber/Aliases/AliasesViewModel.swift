import SwiftUI
import Combine
import DependencyInjection


final class AliasesViewModel: ObservableObject {
    
    @Published var aliases: [String]
    
    private let store: UserDefaults
    @InjectedObservedObject private var spaces: SpaceIndexViewModel
    
    private var cancellables = Set<AnyCancellable>()

    init(
        store: UserDefaults = .standard
    ) {
        self.store = store
        self.aliases = [String]()
        
        self.aliases = self.loadAliases() ?? Self.defaultAliases
        
        $aliases
            .sink { aliases in
                self.saveAliases(aliases)
            }
            .store(in: &cancellables)
        
        spaces.$spacesCount.sink { spacesCount in
            guard spacesCount > self.aliases.count else {
                return
            }
            for newAliasIndex in self.aliases.count..<spacesCount {
                self.aliases.append("\(newAliasIndex + 1)")
            }
        }
        .store(in: &cancellables)
    }
}

// MARK: - Defaults

extension AliasesViewModel {
    static private var defaultAliases: [String] {
        (0..<1)
            .reduce(into: [String]()) { aux, index in
                aux.append("\(index + 1)")
        }
    }
}

// MARK: - Persistence

extension AliasesViewModel {
    private func loadAliases() -> [String]? {
        return (store.array(forKey: Constants.aliasesSaveKey) as? [String])?.emptyToNil
    }
    
    private func saveAliases(_ aliases: [String]? = nil) {
        store.set(aliases ?? self.aliases, forKey: Constants.aliasesSaveKey)
    }
}

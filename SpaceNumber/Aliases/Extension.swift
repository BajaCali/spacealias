import Foundation

extension Array {
    public subscript(safe index: Int) -> Element? {
        guard index < endIndex && index >= 0 else {
            return nil
        }
        return self[index]
    }
}

extension Array {
    public var emptyToNil: Self? {
        if self.isEmpty {
            return nil
        }
        return self
    }
}

extension Optional where Wrapped == String {
    public var emptyToNil: Self {
        if
            let self,
            !self.isEmpty
        {
            return self
        }
        return nil
    }
}

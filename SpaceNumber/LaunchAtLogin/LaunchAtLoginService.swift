import ServiceManagement

class LaunchAtLoginService: LaunchAtLoginToggleProviding {

    var launchAtLoginToggle: Bool {
        get { SMAppService.mainApp.status == .enabled }
        set {
            do {
                if newValue {
                    try SMAppService.mainApp.register()
                } else {
                    try SMAppService.mainApp.unregister()
                }
            } catch {
                print(error)
            }
        }
    }

    func updateLaunchAtLogin(isOn: Bool) {
        self.launchAtLoginToggle = isOn
    }
}

protocol LaunchAtLoginToggleProviding {
    var launchAtLoginToggle: Bool { get}
    func updateLaunchAtLogin(isOn: Bool)
}

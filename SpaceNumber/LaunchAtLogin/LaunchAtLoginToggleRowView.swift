import SwiftUI
import DependencyInjection

struct LaunchAtLoginToggleRowView: View {
    @InjectedObservedObject private var launchAtLogin: LaunchAtLoginViewModel
    
    var body: some View {
        Toggle(isOn: $launchAtLogin.launchAtLoginIsEnabled) {
            Text("Launch at Login")
                .foregroundColor(.secondary)
        }
    }
}

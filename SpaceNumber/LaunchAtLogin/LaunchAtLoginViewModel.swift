import SwiftUI
import DependencyInjection

final class LaunchAtLoginViewModel: ObservableObject {
    @Injected private var launchAtLoginService: LaunchAtLoginToggleProviding

    @Published var launchAtLoginIsEnabled: Bool {
        willSet {
            launchAtLoginService.updateLaunchAtLogin(isOn: newValue)
        }
    }

    init() {
        self.launchAtLoginIsEnabled = false
        self.launchAtLoginIsEnabled = launchAtLoginService.launchAtLoginToggle
    }
}

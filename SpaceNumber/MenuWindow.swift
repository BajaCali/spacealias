import SwiftUI
import Cocoa
import DependencyInjection

struct MenuWindow: View {
    
    @State private var text = ""
    @InjectedObservedObject private var aliases: AliasesViewModel
    
    var body: some View {
        VStack(alignment: .center) {
            AliasesSettingsView()

            Divider()
            
            HStack {
                LaunchAtLoginToggleRowView()

                Spacer()

                Button {
                    NSApplication.shared.terminate(nil)
                } label: {
                    Text("Quit SpaceAlias")
                        .foregroundColor(.secondary)
                }
                .buttonStyle(.borderless)
            }
        }
        .padding([.horizontal, .bottom])
        .padding(.top, 10)
    }
}


struct MenuWindow_Previews: PreviewProvider {
    @StateObject static private var aliases = AliasesViewModel()
    static var previews: some View {
        MenuWindow()
    }
}

import SwiftUI
import DependencyInjection

@main
struct SpaceNumberApp: App {
    init() {
        Container.shared.register(type: LaunchAtLoginToggleProviding.self, dependency: LaunchAtLoginService())
        Container.shared.register(type: SpacesInfoProviding.self, dependency: DisplaysSpacesPublisher())
        Container.shared.register(dependency: LaunchAtLoginViewModel())
        Container.shared.register(dependency: SpaceIndexViewModel())
        Container.shared.register(dependency: AliasesViewModel())
    }
    
    var body: some Scene {
        MenuBarExtra {
            MenuWindow()
        } label: {
            SpacePresenter()
        }
        .menuBarExtraStyle(.window)
    }
}

import Combine
import SwiftUI
import DependencyInjection

class SpaceIndexViewModel: ObservableObject {
    @Published var currentSpaceIndex = 0
    @Published var spacesCount: Int = 0

    @Injected private var spacePublisher: SpacesInfoProviding
    
    private var cancellables = Set<AnyCancellable>()
    
    init() {
        spacePublisher.mainDisplaySpacesPublisher
            .compactMap { $0 }
            .sink(receiveValue: self.updateSpaces(mainDisplay:))
            .store(in: &cancellables)
    }

    func updateSpaces(mainDisplay: Display) {
        if let number = mainDisplay.spaces.firstIndex(of: mainDisplay.currentSpace) {
            currentSpaceIndex = number 
        }
        spacesCount = mainDisplay.spaces.count
    }
}

import SwiftUI
import DependencyInjection

struct SpacePresenter: View {
    @InjectedObservedObject var spaces: SpaceIndexViewModel
    @InjectedObservedObject var aliases: AliasesViewModel
    
    var body: some View {
        Text(presentedString)
    }

    private var presentedString: String {
        aliases.aliases[safe: spaces.currentSpaceIndex].emptyToNil
             ?? String(spaces.currentSpaceIndex + 1)
    }
    
}

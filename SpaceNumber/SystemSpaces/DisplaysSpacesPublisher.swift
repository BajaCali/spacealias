import Cocoa
import Combine

final class DisplaysSpacesPublisher: SpacesInfoProviding {
    
    /// Array of displays with spaces configuration.
    @Published var displaysSpaces = [Display]()
    var displaysSpacesPublisher: Published<[Display]>.Publisher { $displaysSpaces }
    /// Main display spaces configuration.
    @Published var mainDisplaySpaces: Display?
    var mainDisplaySpacesPublisher: Published<Display?>.Publisher { $mainDisplaySpaces }

    private let workspace: NSWorkspace
    private let conn = _CGSDefaultConnection()
    

    init(
        workspace: NSWorkspace = .shared
    ) {
        self.workspace = workspace

        self.updateSpaces()

        workspace.notificationCenter.addObserver(
            forName: NSWorkspace.activeSpaceDidChangeNotification,
            object: workspace,
            queue: nil,
            using: receive(notification:)
        )
    }

    private func receive(notification: Notification) {
        guard notification.name == NSWorkspace.activeSpaceDidChangeNotification else {
            return
        }

        updateSpaces()
    }

    func updateSpaces() {
        if let (displays, mainDisplay) =
                Self.getDisplaysSpaces(conn: conn) {
            self.displaysSpaces = displays
            self.mainDisplaySpaces = mainDisplay
        }
    }

    
    private static func getDisplaysSpaces(conn: Int32)
    -> (allDisplays: [Display], mainDisplay: Display)? {
        if
            let displays = (CGSCopyManagedDisplaySpaces(conn) as? [NSDictionary])?
                .reduce(into: Optional<Array<Display>>([]), {aux, displayDictionary in
                    if let display = Display(dictionary: displayDictionary) {
                        aux?.append(display)
                    }
                    else {
                        aux = nil
                    }
                }),
            let mainDisplay = displays.first(where: { $0.displayIdentifier == "Main" }) {
            return (displays, mainDisplay)
        }
        return nil
    }
}


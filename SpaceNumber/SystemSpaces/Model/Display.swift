import Cocoa

struct Display {
    let displayIdentifier: String
    let currentSpace: Space
    let spaces: [Space]
}

extension Display {
    init?(dictionary: NSDictionary) {
        guard
            let currentSpace = (dictionary["Current Space"] as? NSDictionary).flatMap(Space.init),
            let identifier = dictionary["Display Identifier"] as? String,
            let spaces = (dictionary["Spaces"] as? [NSDictionary])?.reduce(into: Optional<Array<Space>>([]), { (aux: inout [Space]?, spaceDictionary) in
                if let space = Space.init(dictionary: spaceDictionary) {
                    aux?.append(space)
                } else {
                    aux = nil
                }
            })
        else {
            return nil
        }
        
        self.displayIdentifier = identifier
        self.currentSpace = currentSpace
        self.spaces = spaces
    }

}

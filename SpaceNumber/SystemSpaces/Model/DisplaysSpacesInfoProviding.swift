import Combine

protocol MainDisplaySpacesInfoProviding {
    var mainDisplaySpacesPublisher: Published<Display?>.Publisher { get }
    var mainDisplaySpaces: Display? { get }
}

protocol DisplaysSpacesInfoProviding {
    var displaysSpaces: [Display] { get }
    var displaysSpacesPublisher: Published<[Display]>.Publisher { get }
}

protocol SpacesInfoProviding: MainDisplaySpacesInfoProviding, DisplaysSpacesInfoProviding {
    func updateSpaces()
}

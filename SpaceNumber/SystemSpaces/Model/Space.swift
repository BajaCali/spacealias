
struct Space {
    let uuid: UUID
    let managedSpaceID: Int
    let type: Int
}

extension Space: Equatable { }

extension Space {
    init?(dictionary: NSDictionary) {
        guard
            let uuid = UUID(uuidString: dictionary["uuid"] as? String ?? ""),
            let managedSpaceID = dictionary["ManagedSpaceID"] as? Int,
            let type = dictionary["type"] as? Int
        else {
            return nil
        }
            
        self.uuid = uuid
        self.managedSpaceID = managedSpaceID
        self.type = type
    }
}

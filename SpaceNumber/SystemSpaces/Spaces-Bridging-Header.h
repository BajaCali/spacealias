//
//  BridgingHeader.h
//  SpaceNumber
//
//  Created by Michal Němec on 22.01.2023.
//

#ifndef BridgingHeader_h
#define BridgingHeader_h

#import <Foundation/Foundation.h>

int _CGSDefaultConnection();
id CGSCopyManagedDisplaySpaces(int conn);
id CGSCopyActiveMenuBarDisplayIdentifier(int conn);


#endif /* BridgingHeader_h */

#!/bin/bash

if [ $(basename $(pwd)) != "SpaceAlias" ] ; then
    echo "Run this script from root folder"
    exit 1
fi

echo 🔨Building archive...
xcodebuild -scheme SpaceAlias-Release -archivePath build/archive archive || exit 1

mkdir build/tmp/
mkdir build/tmp/SpaceAlias/
mv build/archive.xcarchive/Products/Applications/SpaceAlias.app build/tmp/SpaceAlias
ln -s /Applications build/tmp/SpaceAlias/Applications

echo 📦 Pakcing into dmg file...
hdiutil create -volname SpaceAlias -srcfolder build/tmp/SpaceAlias/ -ov -format UDZO build/SpaceAlias.dmg

rm -rf build/tmp
rm -rf build/archive.xcarchive


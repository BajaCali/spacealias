#+TITLE: Developer's documentation

* Release process

This project is realeased on homebrew or can be downloaded as dmg file
to install manually.


** 1. Create application dmg image

Run ~./command_line_tools/release.sh~ from project root
folder. Upload/push to origin.

** 2. Release on GitLab

1. Create tag with next version.
2. Copy url for the dmg image from the tagged version.
3. Create new release and include the dmg filepath as Image.

** 3. Update Cask information on GitHub

[[https://github.com/BajaCali/homebrew-SpaceAlias][Link to GitHub homebrew-SpaceAlias repo]].

1. Copy url for the dmg ime from the tagged version.
2. Run ~brew create --cask <url-to-dmg> --set-name SpaceAlias~ and
   exit from editor saving the file.
3. Run ~brew install --cask SpaceAlias~ it should tell you new sha.
4. Edit the cask with ~brew edit --cask spacealias~. Update version,
   everything else take from previous version.
5. Commit & push to GitHub repo.


* TODOs


** DONE Update only on clic in menu, not periodically

** DONE Login item

** DONE Focus on current desktop on change

** TODO Update codebase with DataModels

 Use current ViewModels as DataModels and create view models with more
 variables and func for views. And try to clean the code as hard as
 you can.

** DONE Use Combine for system apis

** DONE Add gitignore

** DONE Alias empty to nil (bug)

Try to remove the alias string. Nothings there. A number should be
there.

** DONE Reaname to SpaceAlias

** TODO Create some tests

- try containers in test
- use License2Chill

** DONE Create homebrew tap

- create it on github but download things from gitlab
- [X] put binary into repo & release on gilab

[[https://betterprogramming.pub/a-step-by-step-guide-to-create-homebrew-taps-from-github-repos-f33d3755ba74][inspiration]]


** TODO Update cask to remove login item when uninstalling


* Dev notes


** Modelling


*printing raw displays:*

#+begin_src

[{
    "Current Space" =     {
        ManagedSpaceID = 3;
        id64 = 3;
        type = 0;
        uuid = "582C5AE0-D57D-4760-989B-213D75E2FC49";
    };
    "Display Identifier" = Main;
    Spaces =     (
                {
            ManagedSpaceID = 3;
            id64 = 3;
            type = 0;
            uuid = "582C5AE0-D57D-4760-989B-213D75E2FC49";
        },
                {
            ManagedSpaceID = 4;
            id64 = 4;
            type = 0;
            uuid = "AADDE123-C3EF-47A8-A561-B40727424825";
        },
                {
            ManagedSpaceID = 5;
            id64 = 5;
            type = 0;
            uuid = "1EF96A02-481F-4AD2-A55C-93AB5AB4FB1D";
        },
                {
            ManagedSpaceID = 6;
            id64 = 6;
            type = 0;
            uuid = "9C88B0AD-D02F-42D7-91BE-1F0F80FE0732";
        },
                {
            ManagedSpaceID = 7;
            id64 = 7;
            type = 0;
            uuid = "419E84F5-D5F4-4E84-A317-FBC161A47F18";
        },
                {
            ManagedSpaceID = 8;
            id64 = 8;
            type = 0;
            uuid = "58BA5069-964B-49CA-B7B1-82BD82E3F028";
        },
                {
            ManagedSpaceID = 9;
            id64 = 9;
            type = 0;
            uuid = "12A2E944-2328-47B4-BDFB-BB437AEB8AFC";
        },
                {
            ManagedSpaceID = 10;
            id64 = 10;
            type = 0;
            uuid = "7AC71418-91AB-4E62-BA39-DEC3C2B91C50";
        },
                {
            ManagedSpaceID = 11;
            id64 = 11;
            type = 0;
            uuid = "A2E4AAD2-38E3-49F0-A60C-75F211E413E6";
        },
                {
            ManagedSpaceID = 12;
            id64 = 12;
            type = 0;
            uuid = "C0F09384-4E0C-4359-AE12-61D354BC9EA5";
        }
    );
}]

#+end_src
